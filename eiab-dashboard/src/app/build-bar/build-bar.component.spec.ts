import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildBarComponent } from './build-bar.component';

describe('BuildBarComponent', () => {
  let component: BuildBarComponent;
  let fixture: ComponentFixture<BuildBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
