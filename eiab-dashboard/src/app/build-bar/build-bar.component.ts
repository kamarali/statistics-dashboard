import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-build-bar',
  templateUrl: './build-bar.component.html',
  styleUrls: ['./build-bar.component.css']
})
export class BuildBarComponent implements OnInit {

  @Input() name: string
  @Input() status: string

  type: string = 'dark'
  isRunning: boolean = false

  constructor() { }

  ngOnInit() {
    switch(this.status) {
      case 'SUCCESS':
        this.type = 'success'
        break
      case 'IN_PROGRESS':
        this.type = 'info'
        this.isRunning = true
        break
      case 'FAILED':
        this.type = 'danger'
        break
      default:
      console.log("The status " + this.status + " is not recognised!")
        break
    }
  }
}
