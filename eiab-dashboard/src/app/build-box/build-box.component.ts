import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-build-box',
  templateUrl: './build-box.component.html',
  styleUrls: ['./build-box.component.css']
})
export class BuildBoxComponent implements OnInit {

  @Input() buildName: string
  @Input() buildOne: string
  @Input() buildTwo: string
  @Input() buildThree: string

  constructor() { }

  ngOnInit() {
    
  }

}
