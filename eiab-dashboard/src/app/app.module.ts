import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuildBoxComponent } from './build-box/build-box.component';
import { BuildBarComponent } from './build-bar/build-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    BuildBoxComponent,
    BuildBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
